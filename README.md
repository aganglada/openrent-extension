# aganlada/openrent-extension

OpenRent extension feature

### Trying it out

First, you will have to clone the project

`git clone git@bitbucket.org:aganglada/openrent-extension.git`

Go to the project folder

`cd openrent-extension`

Then,

* Open Google Chrome
* Access `chrome://extensions/`
* Click on "Load unpacked extension..."
* Choose the root folder of `openrent-extension` and click on select

It will automatically load the extension and add its respective icon in the right side of the toolbar

Go to `https://www.openrent.co.uk` and click on the icon.

### What features does this extension adds

I tried to give more visibility to the property details.

So, ones you enter `https://www.openrent.co.uk` and search for `London` (for example), 
you'll choose the property you like and go for more details.

There is where magic stars, in **property detail page** you can see that...

* Header is smaller and more organized
* Photos are smaller to give more importance to description and features (user will look at photos before entering the specific property page)
* Sidebar is fixed when you scroll, so you see it all the time.

Hope you like it :)