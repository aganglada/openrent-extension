(function() {
    'use strict';

    var documentEl = document.documentElement,
        header = document.querySelector('header'),
        sidebar = document.querySelector('#rightInfoBar');

    //header
    header.querySelector('.float-left').classList.remove('float-left');
    header.querySelector('.float-right').classList.remove('float-right');

    // fixed sidebar
    window.addEventListener('scroll', function() {
        var clientYOffset = (window.pageYOffset || documentEl.scrollTop)  - (documentEl.clientTop || 0);

        if (clientYOffset >= sidebar.scrollWidth) {
            sidebar.classList.add('fixed');
        } else {
            sidebar.classList.remove('fixed');
        }
    });
})();